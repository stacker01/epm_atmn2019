//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.01.29 at 05:42:29 PM MSK 
//


package by.bsut.petrushenko.task07_1.by.pvst.old_crds;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Cards complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cards">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="thema" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="year" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
 *         &lt;element name="author" type="{http://www.pvst.by/old_crds}Author"/>
 *       &lt;/sequence>
 *       &lt;attribute name="type" default="congratulatory">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="congratulatory"/>
 *             &lt;enumeration value="promotional"/>
 *             &lt;enumeration value="usual"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="country" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="valuable" default="thematic">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="historical"/>
 *             &lt;enumeration value="thematic"/>
 *             &lt;enumeration value="collection"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cards", propOrder = {
    "name",
    "thema",
    "year",
    "author"
})
public class Cards {

    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String thema;
    @XmlElement(required = true)
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger year;
    @XmlElement(required = true)
    protected Author author;
    @XmlAttribute(name = "type")
    protected String type;
    @XmlAttribute(name = "country", required = true)
    protected String country;
    @XmlAttribute(name = "valuable")
    protected String valuable;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the thema property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getThema() {
        return thema;
    }

    /**
     * Sets the value of the thema property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setThema(String value) {
        this.thema = value;
    }

    /**
     * Gets the value of the year property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getYear() {
        return year;
    }

    /**
     * Sets the value of the year property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setYear(BigInteger value) {
        this.year = value;
    }

    /**
     * Gets the value of the author property.
     * 
     * @return
     *     possible object is
     *     {@link Author }
     *     
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Sets the value of the author property.
     * 
     * @param value
     *     allowed object is
     *     {@link Author }
     *     
     */
    public void setAuthor(Author value) {
        this.author = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        if (type == null) {
            return "congratulatory";
        } else {
            return type;
        }
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the valuable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValuable() {
        if (valuable == null) {
            return "thematic";
        } else {
            return valuable;
        }
    }

    /**
     * Sets the value of the valuable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValuable(String value) {
        this.valuable = value;
    }

}

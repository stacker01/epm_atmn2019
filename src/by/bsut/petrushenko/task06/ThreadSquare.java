package by.bsut.petrushenko.task06;



import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Created by vcnuv on 07.06.2018.
 */
public class ThreadSquare implements Runnable {
    private final boolean horVert;
    private int x, y;
    private int direction = 1;
    JButton button;
    private final Container contentPane;
    private static HashSet<ThreadSquare> threadSquares = new HashSet<>();
    private WaitingClass waitingClass;

    public ThreadSquare(boolean horVert, WaitingClass waitingClass) {
        this.waitingClass = waitingClass;
        this.horVert = horVert;
        if (horVert) {
            x = 0;
            y = Settings.POINT_CROSS_ROAD.y + (int) (Math.random() * (Settings.DIMENSION_CROSS_ROAD.height - Settings.HEIGHT_SQUARE));
        } else {
            x = Settings.POINT_CROSS_ROAD.x + (int) (Math.random() * (Settings.DIMENSION_CROSS_ROAD.width - Settings.WIDTH_SQUARE));
            y = 0;
        }
        this.contentPane = Settings.contentPane;
        initButton();
    }

    @Override
    public void run() {
        while (true) {
            while (Settings.numberTask == true && isOnCrossroad()) {//TASK 6.1
                if (threadSquares.isEmpty()) {

                    threadSquares.add(this);

                    iGoThroughCrossRoad(this);
                } else {

                    try {
                        waitingClass.iMustWait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }



            }
            while (Settings.numberTask == false && isOnCrossroad()){//TASK 6.2
                if (this.canGoThrough()) {

                        threadSquares.add(this);

                    iGoThroughCrossRoad(this);
                } else {

                        try {
                            waitingClass.iMustWait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                    }
                }
            }
            if (!isOnCrossroad()) {
                move();
                getNextPoint();
            }
        }
    }



    public static void removeThreadOfList(ThreadSquare threadSquare){
        synchronized (threadSquares){
            threadSquares.remove(threadSquare);
        }
    }

    private boolean isOnCrossroad(){
        if (this.x >= (Settings.POINT_CROSS_ROAD.x - Settings.WIDTH_SQUARE) &&
                this.x <= (Settings.POINT_CROSS_ROAD.x + Settings.DIMENSION_CROSS_ROAD.width) &&
                this.y >= (Settings.POINT_CROSS_ROAD.y - Settings.HEIGHT_SQUARE) &&
                this.y <= (Settings.POINT_CROSS_ROAD.y + Settings.DIMENSION_CROSS_ROAD.height))
            return true;
        return false;

    }

    private void iGoThroughCrossRoad(ThreadSquare threadSquare) {
        while (isOnCrossroad()) {
            move();
            getNextPoint();
        }
//        synchronized (waitingClass) {
            waitingClass.deleteMeWithNotify(threadSquare);

//        }
    }
    private Color getColorOfAnyTh() {
        synchronized (threadSquares) {
            if (threadSquares.iterator().hasNext()) {
                return threadSquares.iterator().next().button.getBackground();
            }
            return Color.white;
        }
    }
    private boolean canGoThrough(){
        synchronized (threadSquares) {
            if (threadSquares.isEmpty() || getColorOfAnyTh().equals(this.button.getBackground())) {
                return true;
            }
        }
        return false;
    }
    private boolean canGoThoughAlone(){
        synchronized (threadSquares){
            return threadSquares.isEmpty();
        }
    }
    public static void cleanList(){
            threadSquares.clear();

    }




    private void getNextPoint() {
        if (horVert) {
            x += Settings.STEP_SQUARE * direction;
            if (x >= Settings.FRAME_WIDTH - Settings.WIDTH_SQUARE) {
                direction = -1;
            }
            if (x <= 0) {
                direction = 1;
            }
        } else {
            y += Settings.STEP_SQUARE * direction;
            if (y >= Settings.FRAME_HEIGHT - Settings.HEIGHT_SQUARE - 24) {
                direction = -1;
            }
            if (y <= 0) {
                direction = 1;
            }
        }
    }

    private void move() {
        button.setBounds(x, y, Settings.WIDTH_SQUARE, Settings.HEIGHT_SQUARE);
        try {
            Thread.sleep(Settings.DELAY_SQUARE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void initButton() {
        Random random = new Random();
        Color[] color = {new Color(0, 0, 0), new Color(255, 0, 0), new Color(0, 255, 0), new Color(255, 165, 122),
                new Color(255, 215, 0), new Color(0, 0, 255), new Color(255, 0, 255), new Color(190, 190, 190)};
        button = new JButton("" + (++Settings.counter));
        button.setLocation(x, y);
        button.setSize(Settings.WIDTH_SQUARE, Settings.HEIGHT_SQUARE);
        button.setMargin(new Insets(0, 0, 0, 0));
        button.setForeground(Settings.COLOR_TEXT_SQUARE);
        button.setBackground(color[random.nextInt(color.length)]);
        contentPane.add(button);
    }
}

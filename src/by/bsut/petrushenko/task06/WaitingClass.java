package by.bsut.petrushenko.task06;

public class WaitingClass {
    public synchronized void deleteMeWithNotify(ThreadSquare threadSquare){
        ThreadSquare.removeThreadOfList(threadSquare);
        this.notifyAll();
//        System.out.println("NOTIFIED  OF "+Thread.currentThread().getName());
    }
    public synchronized void iMustWait() throws InterruptedException {
        this.wait();
    }

}

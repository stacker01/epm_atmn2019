package by.bsut.petrushenko.task5_1.parts;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Parts implements Comparable {
    private int t1;
    private int t2;

    /**
     * Конструкторы принимают время t1 и t2
     * если время одно, то t1=t2=t
     * по умолчанию t=100
     *
     * @param t1
     * @param t2
     */

    public Parts(int t1, int t2) {
        this.t1 = t1;
        this.t2 = t2;

    }

    public Parts(int t) {
        this(t, t);
    }


    public Parts() {
        this(100);
    }

    public int getT1() {
        return t1;
    }

    public int getT2() {
        return t2;
    }

    public void setT1(int t1) {
        this.t1 = t1;
    }

    public void setT2(int t2) {
        this.t2 = t2;
    }

    @Override
    public String toString() {
        return "(" + t1 + " - " + t2 + ")";
    }


    @Override
    public int compareTo(Object o) {
        Parts obj = (Parts) o;
        if (this.t1 <= this.t2)
            if (obj.t1 <= obj.t2) return this.t1 - obj.t1;
            else return -1;
        else if (obj.t1 <= obj.t2) return 1;
        else return obj.t2 - this.t2;

    }

    public static int[] calculateTime(ArrayList<Parts> parts) {
        int sum1 = 0;
        int sum2 = 0;
        int sumSecondFree = 0;
        for (Parts part : parts
        ) {
            sum1 += part.t1;
            if (sum2 < sum1) {
                sumSecondFree +=sum1-sum2;
                sum2 = sum1;
            }
            sum2 += part.t2;
        }
        return new int[]{sum2,sumSecondFree};
    }
    public static ArrayList<Parts> make(String filePath){
        ArrayList<Parts> parts = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(filePath)));
            String line;

            while ((line = reader.readLine()) != null) {
                String[] times = line.split(" ") ;
                parts.add(new Parts(Integer.parseInt(times[0]),Integer.parseInt(times[1])));

            }

        } catch (IOException e) {
            System.out.println(e);
        }
        return parts;
    }

}

package by.bsut.petrushenko.task5_1;

import by.bsut.petrushenko.task5_1.parts.Parts;

import java.util.ArrayList;
import java.util.Collections;

/**
 * МЕТОД ДЖОНСОНА РЕАЛИЗУЕТСЯ В МЕТОДЕ compareTo класса Parts!!!
 * time of Second machine without work - время простоя второго станка
 */

public class JohnsonDemo {
    static final String FILE_PATH = "D:\\EPAM\\repo\\epm_atmn2019\\src\\by\\bsut\\petrushenko\\task5_1\\tests\\test1.txt";

    public static void main(String[] args) {
        int[] times = new int[2];
        ArrayList<Parts> parts = Parts.make(FILE_PATH);
        times = Parts.calculateTime(parts);
        System.out.println(parts);
        System.out.println("time:" + times[0] + "\ntime of Second machine without work: " + times[1]);
        Collections.sort(parts);
        times = Parts.calculateTime(parts);
        System.out.println(parts);
        System.out.println("time:" + times[0] + "\ntime of Second machine without work: " + times[1]);
    }
}

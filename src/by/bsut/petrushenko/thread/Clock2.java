class Clock2 implements Runnable
{
    public void run()
    {
        Thread current = Thread.currentThread();
        while (!current.isInterrupted())
        {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            System.out.println("Tik");
        }
    }
    public static void main(String[] args)
    {
        Clock2 clock = new Clock2();
        Thread clockThread = new Thread(clock);
        clockThread.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        clockThread.interrupt();
    }
}


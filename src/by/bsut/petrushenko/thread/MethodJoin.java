class MethodJoin implements Runnable
{
    private String name;
    public MethodJoin(String name)
    {
        this.name = name;
    }
    public void run()
    {
        System.out.println("I’m " + this.name);
    }
    public static void main(String[] args) throws InterruptedException
    {
        Printer printer1 = new Printer("EPSON");
        Thread thread1 = new Thread(printer1);
        thread1.start();
        try {
            thread1.join();
        } catch (InterruptedException ex) {
            System.out.println("Return to main");
        }
            System.out.println("Return to main");
    }
}


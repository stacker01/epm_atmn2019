public class Printer extends Thread {
    private String name;
    public Printer(String name)
    {
        this.name = name;
    }
    public void run()
    {
        System.out.println("I’m " + this.name);
    }

    public static void main(String[] args)
    {
        Printer printer = new Printer("Epson");
        printer.start();

        Printer printer2 = new Printer("HP");
        printer2.start();

    }
}


package by.bsut.petrushenko.task3.box;

import java.io.*;

public class FileHandler {
    public static final String FILE_PATH = "save.bin";

    public static void saveToFile(Box box){
        File file = new File(FILE_PATH);
        try (ObjectOutputStream objectOutputStream= new ObjectOutputStream(new FileOutputStream(file))){
            objectOutputStream.writeObject(box);
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    public static Box loadFromFile(){
        Box box = null;
        File file = new File(FILE_PATH);
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))){
            box = (Box)objectInputStream.readObject();

        }catch (IOException e){
            e.printStackTrace();
        }
        catch (ClassNotFoundException e){
            e.printStackTrace();
        }
        return box;
    }
}

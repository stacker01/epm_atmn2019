package by.bsut.petrushenko.task3.film;

import by.bsut.petrushenko.task3.figure.Figure;
import by.bsut.petrushenko.task3.figure.Triangle;

import java.io.Serializable;

public class FilmTriangle extends Triangle implements Film {
    public FilmTriangle (double length){
        super(length);
    }
    public FilmTriangle (Film filmFigure){
        super((Figure) filmFigure);
    }
}

package by.bsut.petrushenko.task3.film;

import by.bsut.petrushenko.task3.figure.Circle;
import by.bsut.petrushenko.task3.figure.Figure;

public class FilmCircle extends Circle implements Film {
    public FilmCircle (double radius){
        super(radius);
    }
    public FilmCircle (Film filmFigure){
        super((Figure)filmFigure);
    }
}

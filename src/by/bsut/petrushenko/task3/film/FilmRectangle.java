package by.bsut.petrushenko.task3.film;

import by.bsut.petrushenko.task3.figure.Figure;
import by.bsut.petrushenko.task3.figure.Rectangle;

public class FilmRectangle extends Rectangle implements Film {
    public FilmRectangle(double length, double width){
        super(length, width);
    }
    public FilmRectangle(Film filmFigure){
        super((Figure)filmFigure);
    }
}

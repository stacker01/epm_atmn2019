package by.bsut.petrushenko.task3.paper;

import java.io.Serializable;

public interface Paper {
    enum Colour {
        RED, YELLOW, GREEN
    }

    Colour getColour();

    void setColour(Colour colour);

    Painted getPainted();

    class Painted implements Serializable {
        private Colour colour;

        public Colour getColourPainted() {
            return colour;
        }

        public void setColourPainted(Colour colour) {
            if (this.colour == null) {
                this.colour = colour;
            }
        }

        @Override
        public String toString() {
            return "colour " + colour;
        }

    }
}

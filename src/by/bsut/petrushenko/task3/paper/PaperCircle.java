package by.bsut.petrushenko.task3.paper;

import by.bsut.petrushenko.task3.figure.Circle;
import by.bsut.petrushenko.task3.figure.Figure;

import java.util.Objects;

public class PaperCircle extends Circle implements Paper {
    private Painted painted = new Painted();

    public PaperCircle(double radius) {
        super(radius);
    }

    public PaperCircle(Paper paperFigure) {
        super((Figure) paperFigure);
        painted = paperFigure.getPainted();
    }

    @Override
    public Colour getColour() {
        return painted.getColourPainted();
    }

    @Override
    public void setColour(Colour colour) {
        painted.setColourPainted(colour);
    }

    @Override
    public Painted getPainted() {
        return painted;
    }

    @Override
    public String toString() {
        return super.toString() + " colour: " + getColour();

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        PaperCircle paperCircle = (PaperCircle) obj;
        return Objects.equals(painted.getColourPainted(), paperCircle.painted.getColourPainted());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(),painted);
    }
}

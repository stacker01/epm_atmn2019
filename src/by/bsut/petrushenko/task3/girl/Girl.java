package by.bsut.petrushenko.task3.girl;

import by.bsut.petrushenko.task3.box.Box;
import by.bsut.petrushenko.task3.box.FileHandler;
import by.bsut.petrushenko.task3.film.FilmCircle;
import by.bsut.petrushenko.task3.film.FilmRectangle;
import by.bsut.petrushenko.task3.film.FilmTriangle;
import by.bsut.petrushenko.task3.paper.Paper;
import by.bsut.petrushenko.task3.paper.PaperCircle;
import by.bsut.petrushenko.task3.paper.PaperTriangle;

public class Girl {
    public static void main(String[] args) throws Exception{
        Box box = null;
        box=Box.getBox(); //Создаем коробку
        FilmTriangle filmTriangle = new FilmTriangle(5);// Создаем треугольник из пленки
        box.addFigure(filmTriangle); // Добавлем его в коробку
        FilmCircle filmCircle = new FilmCircle(filmTriangle);
        box.addFigure(filmCircle);
        box.addFigure(filmCircle);// Проверяем невозможность добавления одного объекта 2 раза
        FilmRectangle filmRectangle = new FilmRectangle(filmCircle);
        box.addFigure(filmRectangle);
        PaperCircle paperCircle = new PaperCircle(5);
        box.addFigure(paperCircle); //
        FilmTriangle filmTriangle1 = new FilmTriangle(5);
        System.out.println(filmTriangle.equals(filmTriangle1));
        PaperCircle paperCircle1 = new PaperCircle(7);
        paperCircle1.setColour(Paper.Colour.GREEN);
        PaperCircle paperCircle2 = new PaperCircle(7);
        box.addFigure(paperCircle2);
        paperCircle2.setColour(Paper.Colour.GREEN);
        paperCircle.setColour(Paper.Colour.RED);
        System.out.println(box);
        FileHandler.saveToFile(box);
        PaperTriangle paperTriangle = new PaperTriangle(5);
        box.addFigure(paperTriangle);
        System.out.println(FileHandler.loadFromFile());
        //System.out.println(box);



    }
}

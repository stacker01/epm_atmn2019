package by.bsut.petrushenko.task5_2.girl;

import by.bsut.petrushenko.task5_2.box.Box;
import by.bsut.petrushenko.task5_2.figure.Figure;
import by.bsut.petrushenko.task5_2.film.FilmCircle;
import by.bsut.petrushenko.task5_2.film.FilmRectangle;
import by.bsut.petrushenko.task5_2.film.FilmTriangle;
import by.bsut.petrushenko.task5_2.paper.Paper;
import by.bsut.petrushenko.task5_2.paper.PaperCircle;

import java.util.Collections;
import java.util.Comparator;

public class Girl {
    public static void main(String[] args) {
        Box box = Box.getBox(); //Создаем коробку
        FilmTriangle filmTriangle = new FilmTriangle(5);// Создаем треугольник из пленки
        box.addFigure(filmTriangle); // Добавлем его в коробку
        FilmCircle filmCircle = new FilmCircle(filmTriangle);
        box.addFigure(filmCircle);
        box.addFigure(filmCircle);// Проверяем невозможность добавления одного объекта 2 раза
        FilmRectangle filmRectangle = new FilmRectangle(filmCircle);
        box.addFigure(filmRectangle);
        PaperCircle paperCircle = new PaperCircle(5);
        box.addFigure(paperCircle); //
        FilmTriangle filmTriangle1 = new FilmTriangle(5);
        System.out.println(filmTriangle.equals(filmTriangle1));
        PaperCircle paperCircle1 = new PaperCircle(7);
        paperCircle1.setColour(Paper.Colour.GREEN);

        /**
         * Comporators:
         */
        System.out.println(box);
        System.out.println();
        System.out.println();

        System.out.println(Box.sortByClassName(box));



    }
}

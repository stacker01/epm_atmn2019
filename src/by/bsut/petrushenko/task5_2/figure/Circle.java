package by.bsut.petrushenko.task5_2.figure;
/**
 * Java abstract class for all circles
 * @autor Vladimir Petrushenko
 * @version 1.0
 */

import java.util.Objects;

public abstract class Circle extends Figure {
    private double radius;

    /**
     * @param radius - Radius of new Circle
     */

    public Circle(double radius) {
        this.radius = radius;
    }

    /**
     * @param figure - every object, which supported class Figure
     */

    public Circle(Figure figure) {
        this.radius = figure.getMinSize();
    }

    /**
     * @return - returning Radius of this Circle
     */

    public double getRadius() {
        return radius;
    }

    /**
     * @param radius - it's new Radius of this Circle. Use this method if you make mistake at the begin
     */

    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * @return - returning size, which you can use for cutting any figure from this Circle
     */

    @Override
    public double getMinSize() {
        return radius / Math.sqrt(2);
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * 2 * radius;
    }

    @Override
    public String toString() {
        return super.toString() + ": radius = " + radius;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) return false;
        if (this == obj) return true;
        Circle circle = (Circle) obj;
        return Double.compare(circle.getRadius(), radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(radius);
    }
}

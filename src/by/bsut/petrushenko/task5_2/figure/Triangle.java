package by.bsut.petrushenko.task5_2.figure;


import java.util.Objects;

public abstract class Triangle extends Figure {
    private double length;
    public Triangle (double length){
        this.length = length;
    }
    public double getRadius(){
        return length;
    }
    public Triangle (Figure figure){
        this.length = figure.getMinSize();
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    @Override
    public double getMinSize() {
        return length/4;
    }

    @Override
    public double getArea() {
        return length*length*Math.sqrt(3.)/4;
    }

    @Override
    public double getPerimeter() {
        return length*3;
    }

    @Override
    public String toString() {
        return super.toString() + ": length = " + length;
    }
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) return false;
        if (this == obj) return true;
        Triangle triangle = (Triangle) obj;
        return Double.compare(triangle.getRadius(), getLength()) == 0;
    }
    @Override
    public int hashCode() {
        return Objects.hashCode(length);

    }
}


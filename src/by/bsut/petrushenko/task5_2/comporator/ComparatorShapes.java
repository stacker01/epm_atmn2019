package by.bsut.petrushenko.task5_2.comporator;


import by.bsut.petrushenko.task5_2.figure.Figure;


import java.util.Comparator;

public class ComparatorShapes implements Comparator<Figure> {
    /**
     * Сортировка по названию класса
     * @param t1
     * @param t2
     * @return
     */

    @Override
    public int compare(Figure t1, Figure t2) {
        return t1.getClass().getSimpleName().compareTo(t2.getClass().getSimpleName());

    }
}
//        public void sortByArea(Box box){
//        box.sort(new Comparator<Figure>(){
//            @Override
//            public int compare(Figure f01, Figure f02){
//                return 0;
//
//            }
//            )
//        }
//    }
//    public static class InnerComparator implements Comparator<Figure> {
//
//        @Override
//        public int compare(Figure figure1, Figure figure2) {
//            return figure1.getColor().compareTo(figure2.getColor());
//        }
//    }
//    box.sort(new InnerComparator)
//}

package by.bsut.petrushenko.task5_2.paper;

import by.bsut.petrushenko.task5_2.figure.Figure;
import by.bsut.petrushenko.task5_2.figure.Triangle;

import java.util.Objects;

public class PaperTriangle extends Triangle implements Paper {
    private Painted painted = new Painted();

    public PaperTriangle(double length) {
        super(length);
    }

    public PaperTriangle(Paper paperFigure) {
        super((Figure) paperFigure);
        painted = paperFigure.getPainted();
    }

    @Override
    public Colour getColour() {
        return painted.getColourPainted();
    }

    @Override
    public void setColour(Colour colour) {
        painted.setColourPainted(colour);
    }

    @Override
    public Painted getPainted() {
        return painted;
    }

    @Override
    public String toString() {
        return super.toString() + " colour: " + getColour();

    }

    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        PaperTriangle paperTriangle = (PaperTriangle) obj;
        return Objects.equals(paperTriangle.painted.getColourPainted(), painted.getColourPainted());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), painted);
    }
}

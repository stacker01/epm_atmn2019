package by.bsut.petrushenko.task5_2.film;
import by.bsut.petrushenko.task5_2.figure.Figure;
import by.bsut.petrushenko.task5_2.figure.Triangle;

public class FilmTriangle extends Triangle implements Film {
    public FilmTriangle (double length){
        super(length);
    }
    public FilmTriangle (Film filmFigure){
        super((Figure) filmFigure);
    }
}

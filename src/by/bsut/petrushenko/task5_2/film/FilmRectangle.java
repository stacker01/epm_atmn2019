package by.bsut.petrushenko.task5_2.film;

import by.bsut.petrushenko.task5_2.figure.Figure;
import by.bsut.petrushenko.task5_2.figure.Rectangle;

public class FilmRectangle extends Rectangle implements Film {
    public FilmRectangle(double length, double width){
        super(length, width);
    }
    public FilmRectangle(Film filmFigure){
        super((Figure)filmFigure);
    }
}

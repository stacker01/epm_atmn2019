package by.bsut.petrushenko.task5_2.film;

import by.bsut.petrushenko.task5_2.figure.Circle;
import by.bsut.petrushenko.task5_2.figure.Figure;

public class FilmCircle extends Circle implements Film {
    public FilmCircle (double radius){
        super(radius);
    }
    public FilmCircle (Film filmFigure){
        super((Figure)filmFigure);
    }
}

package by.bsut.petrushenko.task01.girl;

import by.bsut.petrushenko.task01.box.Box;
import by.bsut.petrushenko.task01.film.FilmCircle;
import by.bsut.petrushenko.task01.film.FilmRectangle;
import by.bsut.petrushenko.task01.film.FilmTriangle;
import by.bsut.petrushenko.task01.paper.Paper;
import by.bsut.petrushenko.task01.paper.PaperCircle;

public class Girl {
    public static void main(String[] args) {
        Box box = Box.getBox(); //Создаем коробку
        FilmTriangle filmTriangle = new FilmTriangle(5);// Создаем треугольник из пленки
        box.addFigure(filmTriangle); // Добавлем его в коробку
        FilmCircle filmCircle = new FilmCircle(filmTriangle);
        box.addFigure(filmCircle);
        box.addFigure(filmCircle);// Проверяем невозможность добавления одного объекта 2 раза
        FilmRectangle filmRectangle = new FilmRectangle(filmCircle);
        box.addFigure(filmRectangle);
        PaperCircle paperCircle = new PaperCircle(5);
        box.addFigure(paperCircle); //
        FilmTriangle filmTriangle1 = new FilmTriangle(5);
        System.out.println(filmTriangle.equals(filmTriangle1));
        PaperCircle paperCircle1 = new PaperCircle(7);
        paperCircle1.setColour(Paper.Colour.GREEN);
        PaperCircle paperCircle2 = new PaperCircle(7);
        paperCircle2.setColour(Paper.Colour.GREEN);
        System.out.println(paperCircle1.equals(paperCircle2));// Проверяем одинкаовость одинаковых фигур
        System.out.println(String.format("%1.3f %1.3f", filmRectangle.getArea(), filmRectangle.getRadius()));
        System.out.println(paperCircle);
        System.out.println(box); // Вывод содержимого коробки до окраски круга
        paperCircle.setColour(Paper.Colour.RED);
        paperCircle.setColour(Paper.Colour.GREEN);// проверка невозможности окраски 2 раза
        System.out.println(box); // Вывод содержимого коробки после окраски круга
        // Проверка методов коробки:
        System.out.println(box.countOfFiguresInTheBox());
        System.out.println(box.indexOfTheSameFigure(paperCircle));
        System.out.println(box.getSummaryArea());
        System.out.println(box.getSummaryPerimeter());
        System.out.println(box.listOfCircles());
        System.out.println(box.listOfFilms());

    }
}

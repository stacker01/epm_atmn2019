package by.bsut.petrushenko.task01.paper;

import by.bsut.petrushenko.task01.figure.Figure;
import by.bsut.petrushenko.task01.figure.Rectangle;

import java.util.Objects;

public class PaperRectangle extends Rectangle implements Paper {
    private Painted painted = new Painted();

    public PaperRectangle(double length, double width) {
        super(length, width);
    }

    public PaperRectangle(Paper paperFigure) {
        super((Figure) paperFigure);
        painted = paperFigure.getPainted();
    }

    @Override
    public Colour getColour() {
        return painted.getColourPainted();
    }

    @Override
    public void setColour(Colour colour) {
        painted.setColourPainted(colour);
    }

    @Override
    public Painted getPainted() {
        return painted;
    }

    @Override
    public String toString() {
        return super.toString() + " colour: " + getColour();

    }

    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!super.equals(obj)) return false;
        PaperRectangle paperRectangle = (PaperRectangle) obj;
        return Objects.equals(painted.getColourPainted(), paperRectangle.painted.getColourPainted());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), painted);
    }
}

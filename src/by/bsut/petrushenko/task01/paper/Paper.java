package by.bsut.petrushenko.task01.paper;

public interface Paper {
    enum Colour {
        RED, YELLOW, GREEN
    }

    Colour getColour();

    void setColour(Colour colour);

    Painted getPainted();

    class Painted {
        private Colour colour;

        public Colour getColourPainted() {
            return colour;
        }

        public void setColourPainted(Colour colour) {
            if (this.colour == null) {
                this.colour = colour;
            }
        }

        @Override
        public String toString() {
            return "colour " + colour;
        }

    }
}

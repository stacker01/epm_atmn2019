package by.bsut.petrushenko.task01.box;

import by.bsut.petrushenko.task01.figure.Circle;
import by.bsut.petrushenko.task01.figure.Figure;
import by.bsut.petrushenko.task01.film.Film;

import java.util.ArrayList;
import java.util.List;

public class Box {

    private static Box box;

    private Box() {

    }

    public static Box getBox() {
        if (box == null) {
            box = new Box();
        }
        return box;
    }

    private final List<Figure> boxShapes = new ArrayList<>();

    public boolean addFigure(Figure figure) {
        if (figure != null && !boxShapes.contains(figure)) {
            boxShapes.add(figure);
            return true;
        }
        return false;
    }

    private boolean checkNumber(int number) {
        return number >= 0 && number < boxShapes.size();
    }

    public Figure getFigureByNumber(int number) {
        if (!checkNumber(number)) return null;
        return boxShapes.get(number);
    }

    public Figure extractFigureByNumber(int number) {
        if (!checkNumber(number)) return null;
        return boxShapes.remove(number);
    }

    /**
     * найти фигуру по образцу (эквивалентную по своим характеристикам)
     * @param figure - can work with any figures
     * @return - returning index of Figure if it exist. If it not exist - returning "-1"
     */
    public int indexOfTheSameFigure(Figure figure) {
        if (boxShapes.contains(figure)) return boxShapes.indexOf(figure);
        return -1;
    }

    /**
     * показать наличное количество фигур,
     * @return - returning count of figures in the box
     */
    public int countOfFiguresInTheBox (){
        return boxShapes.size();
    }

    /**
     * показать суммарную площадь
     */
    public double getSummaryArea(){
        Double area=0.;
        for (Figure figure:boxShapes
             ) {
            area+=figure.getArea();
        }
        return area;
    }

    /**
     * суммарный периметр
     * @return
     */
    public double getSummaryPerimeter(){
        Double perimeter=0.;
        for (Figure figure:boxShapes
        ) {
            perimeter+=figure.getPerimeter();
        }
        return perimeter;
    }

    /**
     * достать все Круги
     * @return
     */
    public List listOfCircles(){
         ArrayList<Circle> circleList = new ArrayList<>();
        for (Figure figure:boxShapes
             ) {
            if (figure instanceof Circle) circleList.add((Circle)figure);
        }
         return circleList;
    }

    /**
     * достать все Пленочные фигуры
     * @return
     */
    public List listOfFilms(){
        ArrayList<Film> filmArrayList = new ArrayList<>();
        for (Figure figure:boxShapes
        ) {
            if (figure instanceof Film) filmArrayList.add((Film) figure);
        }
        return filmArrayList;
    }


    @Override
    public String toString() {
        return "Box of shapes: " + boxShapes;
    }
}


package by.bsut.petrushenko.task01.film;

import by.bsut.petrushenko.task01.figure.Circle;
import by.bsut.petrushenko.task01.figure.Figure;
import by.bsut.petrushenko.task01.film.Film;

public class FilmCircle extends Circle implements Film {
    public FilmCircle (double radius){
        super(radius);
    }
    public FilmCircle (Film filmFigure){
        super((Figure)filmFigure);
    }
}

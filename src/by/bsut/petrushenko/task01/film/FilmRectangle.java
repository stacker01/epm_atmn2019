package by.bsut.petrushenko.task01.film;

import by.bsut.petrushenko.task01.figure.Figure;
import by.bsut.petrushenko.task01.figure.Rectangle;
import by.bsut.petrushenko.task01.film.Film;

public class FilmRectangle extends Rectangle implements Film {
    public FilmRectangle(double length, double width){
        super(length, width);
    }
    public FilmRectangle(Film filmFigure){
        super((Figure)filmFigure);
    }
}

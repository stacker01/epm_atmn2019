package by.bsut.petrushenko.task01.film;
import by.bsut.petrushenko.task01.figure.Figure;
import by.bsut.petrushenko.task01.figure.Triangle;

public class FilmTriangle extends Triangle implements Film {
    public FilmTriangle (double length){
        super(length);
    }
    public FilmTriangle (Film filmFigure){
        super((Figure) filmFigure);
    }
}

package by.bsut.petrushenko.inAuditorium;

class CallMe{
     void call(String msg){
        System.out.print("["+ msg);
        try {
            Thread.sleep(1000);
        }catch (InterruptedException e){
            System.out.println("Interrupted");
        }
        System.out.println("]");
    }

}


class Caller implements Runnable{
    String msg;
    CallMe target;
    Thread t;
    public Caller(CallMe targ, String s){
        target = targ;
        msg = s;
        t= new Thread(this);
        t.start();
    }
    public void run(){
        synchronized (target) {
            target.call(msg);
        }
    }
}

public class Example {
    public static void main(String[] args) {

        CallMe target = new CallMe();
//        synchronized (target) {
            Caller ob1 = new Caller(target, "Welcome ");
//        try {
//            ob1.t.join();
//        Thread.sleep(1001);
            Caller ob2 = new Caller(target, "in syncronized");
//            ob2.t.join();
//        Thread.sleep(1001);
            Caller ob3 = new Caller(target, "world");

            try {
                ob1.t.join();
                ob2.t.join();
                ob3.t.join();
            } catch (InterruptedException e) {
                System.out.println("Interrupted");
            }
//        }
    }
}
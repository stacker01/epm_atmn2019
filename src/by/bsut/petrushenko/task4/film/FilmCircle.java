package by.bsut.petrushenko.task4.film;

import by.bsut.petrushenko.task4.figure.Circle;
import by.bsut.petrushenko.task4.figure.Figure;

public class FilmCircle extends Circle implements Film {
    public FilmCircle (double radius){
        super(radius);
    }
    public FilmCircle (Film filmFigure){
        super((Figure)filmFigure);
    }
}

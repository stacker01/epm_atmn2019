package by.bsut.petrushenko.task4.film;

import by.bsut.petrushenko.task4.figure.Figure;
import by.bsut.petrushenko.task4.figure.Rectangle;

public class FilmRectangle extends Rectangle implements Film {
    public FilmRectangle(double length, double width){
        super(length, width);
    }
    public FilmRectangle(Film filmFigure){
        super((Figure)filmFigure);
    }
}

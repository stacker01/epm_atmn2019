package by.bsut.petrushenko.task4.exceptions;

public class IndexOfBoundsException extends RuntimeException {
    public IndexOfBoundsException(){

    }
    public IndexOfBoundsException(String message){
        super(message);
    }
}

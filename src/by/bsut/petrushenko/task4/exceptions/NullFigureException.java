package by.bsut.petrushenko.task4.exceptions;

public class NullFigureException extends Exception {
    public NullFigureException() {
    }

    public NullFigureException(String message) {
        super(message);
    }
}

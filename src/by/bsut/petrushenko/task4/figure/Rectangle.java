package by.bsut.petrushenko.task4.figure;


import java.util.Objects;

public abstract class Rectangle extends Figure {
    private double length;
    private double width;
    private final int HASH_CONSTANT = 31;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public Rectangle(Figure figure) {
        this.length = figure.getMinSize();
        this.width = figure.getMinSize();
    }

    public double getRadius() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    @Override
    public double getMinSize() {
        return Math.min(width, length) / 2;
    }

    @Override
    public double getArea() {
        return length * width;
    }

    @Override
    public double getPerimeter() {
        return (length + width) * 3;
    }

    @Override
    public String toString() {
        return super.toString() + ": length = " + length + ", width = " + width;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) return false;
        if (this == obj) return true;
        Rectangle rectangle = (Rectangle) obj;
        return Double.compare(rectangle.getLength(), length) == 0 && Double.compare(rectangle.getWidth(), width) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(length * 31 + width);
    }
}


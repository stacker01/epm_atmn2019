package by.bsut.petrushenko.task4.figure;
/**
 * Java abstract class for all geometric figures
 * @autor Vladimir Petrushenko
 * @version 1.0
 */
public abstract class Figure {
    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract double getMinSize();
    @Override
    public String toString(){
        return '\n' + this.getClass().getSimpleName();
        //getSimpleName - название класса
    }

    @Override
    public boolean equals(Object obj) {
        if (obj==null){return false;}
        return this.getClass()==obj.getClass();
    }
}
